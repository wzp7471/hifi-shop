package com.jshop.action.app;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;

import com.alibaba.fastjson.JSON;
import com.jshop.action.backstage.utils.statickey.StaticKey;
import com.jshop.entity.ArticleCategoryT;
import com.jshop.entity.ArticleT;
import com.jshop.service.ArticleCategoryTService;
import com.jshop.service.ArticleTService;
import com.jshop.vo.PageBean;

@Namespace("/app/article")
public class ArticleApiAction extends BaseApiAction {

    private static final long serialVersionUID = 4757947271002473018L;

    @Resource
    private ArticleTService articleTService;
    @Resource
    private ArticleCategoryTService articleCategoryTService;

    private String grade;
    private String categoryId;
    private String articleId;
    private String flagType;
    private int currentPage;
    private int lineSize;

    public ArticleTService getArticleTService() {
        return articleTService;
    }

    public void setArticleTService(ArticleTService articleTService) {
        this.articleTService = articleTService;
    }

    public ArticleCategoryTService getArticleCategoryTService() {
        return articleCategoryTService;
    }

    public void setArticleCategoryTService(ArticleCategoryTService articleCategoryTService) {
        this.articleCategoryTService = articleCategoryTService;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getFlagType() {
        return flagType;
    }

    public void setFlagType(String flagType) {
        this.flagType = flagType;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getLineSize() {
        return lineSize;
    }

    public void setLineSize(int lineSize) {
        this.lineSize = lineSize;
    }

    /**
     * 根据层级，查找对应的文章分类 参数：grade(从0开始)
     */
    @Action(value = "findCategoryByGrade")
    public void findCagetoryByGrade() {
        List<ArticleCategoryT> list = articleCategoryTService
                .findArticleCategoryByGrade(this.getGrade(), StaticKey.ONE);
        String json = JSON.toJSONString(list, true);
        this.flush2Client(json);
    }

    /**
     * 根据父id，查找子分类 参数：categoryId
     */
    @Action(value = "findCategoryByParent")
    public void findCategoryByParentId() {
        List<ArticleCategoryT> list = articleCategoryTService.findArticleCategoryByparentId(StaticKey.ONE,
                this.getCategoryId());
        String json = JSON.toJSONString(list, true);
        this.flush2Client(json);
    }

    /**
     * 根据品类id和品类层级查找文章 参数：categoryId, grade(从0开始)
     */
    @Action(value = "findArticleByCategory")
    public void findArticleByCategory() {
        PageBean<ArticleT> pageBean = new PageBean<ArticleT>(this.getCurrentPage(), this.getLineSize());
        pageBean.addParam("grade", this.getGrade());
        pageBean.addParam("categoryId", this.getCategoryId());
        pageBean.addParam("status", StaticKey.ONE);
        List<ArticleT> list = articleTService.findArticlesByStypeidWithPage(pageBean);
        String json = JSON.toJSONString(list, true);
        this.flush2Client(json);
    }

    /**
     * 根据文章ID，查找指定文章 参数：articleId
     */
    @Action(value = "findArticleById")
    public void findArticleById() {
        ArticleT article = articleTService.findByPK(ArticleT.class, this.getArticleId());
        String json = JSON.toJSONString(article, true);
        this.flush2Client(json);
    }

    /**
     * 根据文章特殊标识，查找指定文章 
     * 参数：flagType 1公告，2发布，3推荐，4置项
     */
    @Action(value = "findArticleByFlagType")
    public void findArticleByFlagType() {
        PageBean<ArticleT> pageBean = new PageBean<ArticleT>(this.getCurrentPage(), this.getLineSize());
        if ("1".equals(this.getFlagType())) {
            pageBean.addParam("isnotice", StaticKey.ONE);
        } else if ("2".equals(this.getFlagType())) {
            pageBean.addParam("ispublication", StaticKey.ONE);
        } else if ("3".equals(this.getFlagType())) {
            pageBean.addParam("isrecommend", StaticKey.ONE);
        } else if ("4".equals(this.getFlagType())) {
            pageBean.addParam("istop", StaticKey.ONE);
        }
        
        List<ArticleT> list = articleTService.findArticlesByStypeidWithPage(pageBean);
        String json = JSON.toJSONString(list, true);
        this.flush2Client(json);
    }
}
