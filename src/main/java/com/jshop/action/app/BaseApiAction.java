package com.jshop.action.app;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("jshop")
@InterceptorRefs({ @InterceptorRef("defaultStack") })
public class BaseApiAction extends ActionSupport implements
        ServletRequestAware, ServletResponseAware {

    private static final long serialVersionUID = 1471838996953390885L;
    private HttpServletRequest request;
    private HttpServletResponse response;

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    @Override
    public void setServletResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    protected void flush2Client(String json) {
        try {
            String cbName = request.getParameter("callback");
            String outStr = json;
            if (StringUtils.isNotEmpty(cbName)) {
                outStr = cbName + "(" + json.toString() + ")";
            }

            response.setContentType("text/json");
            response.setCharacterEncoding("utf-8");
            PrintWriter out = response.getWriter();
            out.write(outStr);
            out.flush();
            out.close();

        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException();
        }
    }

}
